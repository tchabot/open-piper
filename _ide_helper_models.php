<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\BlogCategory
 *
 */
	class BlogCategory extends \Eloquent {}
}

namespace App{
/**
 * App\BlogPost
 *
 */
	class BlogPost extends \Eloquent {}
}

namespace App{
/**
 * App\Lesson
 *
 */
	class Lesson extends \Eloquent {}
}

namespace App{
/**
 * App\LessonCategory
 *
 */
	class LessonCategory extends \Eloquent {}
}

namespace App{
/**
 * App\LessonContent
 *
 */
	class LessonContent extends \Eloquent {}
}

namespace App{
/**
 * App\LessonSupport
 *
 */
	class LessonSupport extends \Eloquent {}
}

namespace App{
/**
 * App\Order
 *
 */
	class Order extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\UserRole
 *
 */
	class UserRole extends \Eloquent {}
}

