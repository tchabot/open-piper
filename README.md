***

<p align="center"><b> :book: Open Piper </b></p>

***

## Introduction

Pied piper is an online platform selling IT formations.

## :rocket: Project installation

### :closed_lock_with_key: Requirements

To run this project, you may have some requirements:
* PHP 7.0
* [Composer](https://getcomposer.org/)
* [Node/npm](https://nodejs.org/en/)

All theses requirements are checked with [Homestead](https://laravel.com/docs/master/homestead) for local dev.

### :wrench: Installation

To run this project, you have to type some commands:

```bash
$ cd openpiper/
$ cp .env.example .env
$ composer install
$ npm install
```

:warning: If `npm install` fails, due using Homestead with Windows host, you must run the following command:

```bash
$ npm install --no-bin-links || npm install --no-bin-links
```

Then, to compile assets, you have to run the following command:

* For production environment:
```bash
$ npm run production
```

* For development environment:
```bash
$ npm run dev
```
