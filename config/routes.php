<?php

return [
    'home' => [
        'controller' => 'Pages\HomeController@index',
        'fr' => [
            'url' => '/',
            'title' => 'Apprenez à programmer',
            'description' => 'Apprendre la programmation n\'aura jamais été aussi facile !',
            'text_500' => 'OpenPiper est une plateforme où l\'on peut apprendre le développement informatique à son rythme et quel que soit son niveau. <br/>Créée par des développeurs et développeuses, pour des développeurs et développeuses en herbe, en phase d\'apprentissage ou en quête de perfectionnement sur une technologie en particulier.Nos cours se décomposent en un ensemble de leçons afin de découper l\'apprentissage d\'un langage en plusieurs étapes et chaque leçon contient des supports soit pdf soit vidéo.'
        ]
    ],
    'profile' => [
        'controller' => 'Pages\ProfileController@index',
        'fr' => [
            'url' => '/profil',
            'title' => 'Profil',
            'description' => 'Vos informations'
        ]
    ],
    'articles' => [
        'controller' => 'Pages\BlogController@index',
        'fr' => [
            'url' => '/blog',
            'title' => 'Blog',
            'description' => 'Tous les articles'
        ]
    ],
    'article_detail' => [
        'controller' => 'Pages\BlogController@detail',
        'fr' => [
            'url' => '/blog/{slug}',
            'title' => '',
            'description' => ''
        ]
    ],
    'products' => [
        'controller' => 'Pages\ProductController@index',
        'fr' => [
            'url' => '/formations',
            'title' => 'Formations',
            'description' => 'Liste des formations'
        ]
    ],
    'product_detail' => [
        'controller' => 'Pages\ProductController@detail',
        'fr' => [
            'url' => '/formations/{slug}',
            'title' => '',
            'description' => ''
        ]
    ],
    'tutorials' => [
        'controller' => 'Pages\TutorialController@index',
        'fr' => [
            'url' => '/tutoriels',
            'title' => 'Tutoriels',
            'description' => 'Liste des tutoriels'
        ]
    ],
    'tutorial_detail' => [
        'controller' => 'Pages\TutorialController@detail',
        'fr' => [
            'url' => '/tutoriels/{slug}',
            'title' => '',
            'description' => ''
        ]
    ],
    'tos' => [
        'controller' => 'Pages\TosController@index',
        'fr' => [
            'url' => '/cgv',
            'title' => 'Conditions générales de Ventes',
            'description' => 'Voici nos conditions générales de ventes pour OpenPiper'
        ]
    ],
    'tou' => [
        'controller' => 'Pages\TouController@index',
        'fr' => [
            'url' => '/cgu',
            'title' => 'Conditions générales d\'Utilisation',
            'description' => 'Voici nos conditions générales d\'utilisation pour OpenPiper'
        ]
    ],
    'faq' => [
        'controller' => 'Pages\FaqController@index',
        'fr' => [
            'url' => '/faq',
            'title' => 'Foire Aux Questions',
            'description' => 'Découvrez les questions fréqeumment posées concernant l\'utilisation d\'OpenPiper'
        ]
    ],
    'contact' => [
        'controller' => 'Pages\ContactController@index',
        'fr' => [
            'url' => '/contact-support',
            'title' => 'Formulaire de contact',
            'description' => 'Besoin d\'aide ? Remplissez notre formuliare de contact et notre support vous répondra au plus vite'
        ]
    ],
    'chapters' => [
        'controller' => 'Pages\ChapterController@index',
        'fr' => [
            'url' => '/formation/{slug}/chapitres',
            'title' => '',
            'description' => ''
        ]
    ],
    'chapter_detail' => [
        'controller' => 'Pages\ChapterController@detail',
        'fr' => [
            'url' => '/formation/{slug_formation}/chapitres/{slug_chapter}',
            'title' => '',
            'description' => ''
        ]
    ]
];
