<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->integer('chapter_id')->unsigned()->nullable();
            $table->integer('tutorial_id')->unsigned()->nullable();
            $table->integer('author_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('supports', function (Blueprint $table) {
            $table->foreign('chapter_id')->references('id')->on('chapters');
            $table->foreign('tutorial_id')->references('id')->on('tutorials');
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supports');
    }
}
