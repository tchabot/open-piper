<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedDecimal('price');
            $table->string('path');
            $table->integer('chapter_id')->unsigned()->nullable();
            $table->integer('tutorial_id')->unsigned()->nullable();
            $table->integer('author_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('attachments', function (Blueprint $table) {
            $table->foreign('chapter_id')->references('id')->on('chapters');
            $table->foreign('tutorial_id')->references('id')->on('tutorials');
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
