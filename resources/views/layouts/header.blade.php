<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
              aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logo" href="{{ route('home') }}">
        <img src="{{ asset('image/logo.png') }}" alt="">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="{{ route('products') }}" class="primary-inverse">Formations</a></li>
        <li><a href="{{ route('tutorials') }}" class="secondary-inverse">Tutoriels</a></li>
        <li><a href="{{ route('articles') }}" class="secondary-inverse">Blog</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
          <li><a href="{{ route('login') }}" class="secondary-inverse">Connexion</a></li>
          <li><a href="{{ route('register') }}" class="primary-inverse">Inscription</a></li>
        @else
          <li><a href="{{ url('/logout') }}" class="primary-inverse">Logout</a></li>
          <li><a href="{{ route('profile')}}" class="primary-inverse">Profil</a></li>
        @endif
      </ul>
    </div>
  </div>
</nav>
