<!doctype html>
<html lang="{{\App::getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
    @include('tags.master')
</head>
<body>
@include('layouts.header')
<div class="page-wrapper">
    @yield('page-content')
</div>
@include('layouts.footer')
<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</body>
</html>
