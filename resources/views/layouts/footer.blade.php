<footer class="bg">
  <div class="container">
    <div class="row">
      <div id="footer-contact" class="col-md-6">
        <span>Contact US</span>
        <ul>
          <li><a href="mailto:openpip3r@gmail.com">openpip3r@gmail.com</a></li>
          <li>
            <a href="https://www.facebook.com/OpenPip3r/" target="_blank" id="facebook"><i class="fa fa-facebook"
                                                                                           aria-hidden="true"></i></a>
            <a href="https://twitter.com/OpenPip3r" target="_blank" id="twitter"><i class="fa fa-twitter"
                                                                                    aria-hidden="true"></i></a>
          </li>
        </ul>
      </div>
      <div id="footer-info" class="col-md-6">
        <div class="col-md-6">
          <span>Legal info</span>
          <ul>
            <li><a href="{{ route('tou') }}">CGU</a></li>
            <li><a href="{{ route('tos') }}">Terms of Sales</a></li>
          </ul>
        </div>
        <div id="footer-help" class="col-md-6">
          <span>Need help ?</span>
          <ul>
            <li><a href="{{ route('contact') }}">Contact Support</a></li>
            <li><a href="{{ route('faq') }}">FAQ</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

