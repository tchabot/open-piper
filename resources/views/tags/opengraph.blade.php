<meta property="og:title" content="{{ getSeo('title') }}" />
<meta property="og:description" content="{{ getSeo('description') }}" />
<meta property="og:site_name" content="Open Piper" />
<meta property="og:image" content="" /> {{-- TODO: get image link --}}
<meta property="og:type" content="website" />