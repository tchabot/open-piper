@include('tags.seo')
@include('tags.opengraph')
@include('tags.favicon')

// Tags executed only in production
@if (env('APP_ENV') !== 'local')
  @include('tags.analytics')
@endif
