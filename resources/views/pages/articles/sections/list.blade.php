<div id="lesson" class="container">

  <h1>{{ getSeo('description') }}</h1>
  <div class="row">
    <div class="col-md-12 items">
      @foreach ($articles as $article)
        <div class="item">
          <div class="row">
            <div class="col-md-4">
              <img src="https://buyasorta.com/wp/wp-content/plugins/kentooz-socializer/images/default.jpg" alt="">
            </div>
            <div class="col-md-8">
              <h2>{{ $article->title }}</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 description">
              <p>{{ str_limit($article->description, 400, '...' ) }}.</p>
              <a href="{{ route('article_detail',  $article->id) }}" class="primary">En savoir plus</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
