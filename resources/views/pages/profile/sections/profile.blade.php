<div id="profile" class="container">
    <form id="profileForm">
        <h2> Vos informations :</h2>
        <div class="form-group">
            <label for="name" class="sr-only">Name</label>
            @if (@isset($user->last_name))
                <input value="{{$user->last_name}}" type="text" id="name" class="form-control" placeholder="Name" name="last_name" required>
            @else
                <input type="text" id="name" class="form-control" placeholder="Name" name="last_name" required>
            @endif
        </div>
        <div class="form-group">
            <label for="firstname" class="sr-only">Firstname</label>
            @if (@isset($user->first_name))
                <input value="{{$user->first_name}}" type="text" id="firstname" class="form-control" placeholder="Firstname" required>
            @else
                <input type="text" id="firstname" class="form-control" placeholder="Firstname" required>
            @endif
        </div>
        <div class="form-group">
            <label for="mail" class="sr-only">Mail</label>
            @if (@isset($user->email))
                <input value="{{$user->email}}" type="text" id="mail" class="form-control" placeholder="mail" required>
            @else
                <input type="text" id="mail" class="form-control" placeholder="mail" required>
            @endif
        </div>
        <button class="primary" type="submit">Enregistrer</button>
        <div class="fright">
            <button class="primary" type="submit">Modifier mot de passe</button>
        </div>
    </form>
</div>