<div id="detail-lesson" class="container">
    <div class="row">
        <div class="col-md-4">
            <img src="https://buyasorta.com/wp/wp-content/plugins/kentooz-socializer/images/default.jpg" alt="">
        </div>
        <div class="col-md-8">
            <h1>{{$product->title}}</h1>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 description">
            <p>{{$product->description}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 access">
            <a href="{{ route('chapters', $product->id) }}" class="secondary">Accès aux leçons</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 share">
            <div class="fb-share-button" data-href="https://benjamindenie.fr/" data-layout="button" data-size="small" data-mobile-iframe="true">
                <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a>
            </div>
            <a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical"></a>
        </div>
    </div>
</div>

<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<script type="text/javascript" src="{{ asset('js/lesson_detail.js') }}"></script>
<div id="fb-root"></div>
