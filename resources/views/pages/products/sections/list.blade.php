<div id="lesson" class="container">
  <h1>{{ getSeo('description') }}</h1>
  <div class="row">
    <div class="col-md-12 items">
      @foreach($products as $product)
        <div class="item">
          <div class="row">
            <div class="col-md-4">
              <img src="https://buyasorta.com/wp/wp-content/plugins/kentooz-socializer/images/default.jpg" alt="">
            </div>
            <div class="col-md-8">
              <h2>{{$product->title}}</h2>
              <p></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 description">
              <p>{{$product->description}}</p>
              <a href="{{ route('product_detail', $product->id) }}" class="primary">En savoir plus</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
