<div id="lesson" class="container">
    <h1>{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12 items">
            @foreach($chapters as $chapter)
                <div class="item">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>{{$chapter->title}}</h2>
                            <p></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 description">
                            <p>{{$chapter->description}}</p>
                            <a href="{{ route('chapter_detail', ['slug_formation' => $chapter->product_id, 'slug_chapter' => $chapter->id ]) }}" class="primary">En savoir plus</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
