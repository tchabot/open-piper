<div id="lesson" class="container">
  <h1>{{ getSeo('description') }}</h1>
  <div class="row">
    <div class="col-md-12 items">
        @forelse ($tutorials as $tuto)
            <div class="item">
              <div class="row">
                <div class="col-md-4">
                  <img src="https://buyasorta.com/wp/wp-content/plugins/kentooz-socializer/images/default.jpg" alt="">
                </div>
                <div class="col-md-8">
                  <h2>{{ $tuto->title }}</h2>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 description">
                  <p>{{ $tuto->description }}</p>
                  <a href="{{ route('tutorial_detail', $tuto->id) }}" class="primary">En savoir plus</a>
                </div>
              </div>
            </div>
        @empty
            <p>{{ __('No tutorials') }}</p>
        @endforelse
    </div>
  </div>
</div>
