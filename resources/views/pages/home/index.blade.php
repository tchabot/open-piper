@extends('layouts.master')

@section('page-content')
@include('pages.home.sections.top')
@include('pages.home.sections.intro')
@include('pages.home.sections.lessons')
@include('pages.home.sections.premium')
@endsection
