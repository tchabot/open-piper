<div id="home-premium" class="">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>{{ __('Abonnement premium') }}</h2>
        <p>{{ __('Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euis mod.Donec sed odio dui.') }}</p>
        <p><a class="secondary" href="#" role="button">{{ __('Abonner vous &raquo;') }}</a></p>
      </div>
    </div>
  </div>
</div>
