<div id="cours" class="container">
  <div class="row">
    <h2>{{ __('Nos derniers cours') }}</h2>
    @foreach ($last_products as $element)
        <div class="col-md-4">
          <h2>{{ $element->title }}</h2>
          <p>{{ str_limit($element->description, 200, '...') }}</p>
          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
          <i class="fa fa-video-camera" aria-hidden="true"></i>
          <p><a class="primary" href="{{ route('product_detail', $element->id) }}" role="button">{{ __('Voir détails &raquo;') }}</a></p>
        </div>
    @endforeach
  </div>
  <hr>
</div>
