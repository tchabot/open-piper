<div id="bandeau" class="jumbotron parallax"
     style="background-image:linear-gradient(to bottom, rgba(255, 255, 255, 0.55), rgba(255, 255, 255, 0.17)), url('{{ asset('image/bandeau.jpg') }}');">
  <div class="container">
    <h1>{{ getSeo('title') }}</h1>
    <p>{{ getSeo('description') }}</p>
    <p><a class="primary" href="#" role="button">{{ __('En savoir plus &raquo;') }}</a></p>
  </div>
</div>
