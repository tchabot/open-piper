@extends('layouts.master')

@section('page-content')
  <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Register</div>
          <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first-name" class="col-md-4 control-label">First name</label>
                <div class="col-md-6">
                  <input id="first-name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required
                         autofocus>
                  @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="last-name" class="col-md-4 control-label">Last name</label>
                <div class="col-md-6">
                  <input id="last-name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                  @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                <div class="col-md-6">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                  @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>
                <div class="col-md-6">
                  <input id="password" type="password" class="form-control" name="password" required>
                  @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                         required>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="{{env('STRIPE_KEY')}}"
                    data-amount="9000"
                    data-name="Open Piper"
                    data-description="Premium"
                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                    data-locale="auto"
                    data-zip-code="false"
                    data-currency="eur">
                  </script>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
