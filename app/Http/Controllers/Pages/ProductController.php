<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        $products = Product::all();
        return view("pages.products.index", [
            'lang' => $this->lang,
            'products' => $products
        ]);
    }

    public function detail($id)
    {
        $product = Product::find($id);
        return view("pages.products.detail", [
            'lang' => $this->lang,
            'product' => $product
        ]);
    }
}
