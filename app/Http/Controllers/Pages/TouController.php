<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class TouController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.tou.index", [
            'lang' => $this->lang
        ]);
    }
}
