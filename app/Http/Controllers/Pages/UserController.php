<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('auth');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.profile.index", [
            'lang' => $this->lang
        ]);
    }

    public function login()
    {
        return view("pages.login.index", [
            'lang' => $this->lang
        ]);
    }

    public function register()
    {
        return view("pages.register.index", [
            'lang' => $this->lang
        ]);
    }
}
