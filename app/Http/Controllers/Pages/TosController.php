<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class TosController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.tos.index", [
            'lang' => $this->lang
        ]);
    }
}
