<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.contact.index", [
            'lang' => $this->lang
        ]);
    }
}
