<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Product;

class HomeController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.home.index", [
            'lang' => $this->lang,
            'last_products' => Product::latest('created_at')->take(3)->get()
        ]);
    }
}
