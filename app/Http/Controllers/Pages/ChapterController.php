<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 13/09/2017
 * Time: 12:03
 */

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Chapter;
use App\Product;
use App\Support;

class ChapterController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('auth');
        $this->lang = \App::getLocale();
    }

    public function index($productId)
    {
        $chapters = Chapter::where('product_id', '=', $productId)->get();
        $title = Product::find($productId)->title;
        return view("pages.chapters.index", [
            'lang' => $this->lang,
            'chapters' => $chapters,
            'title' => $title
        ]);
    }

    public function detail($id, $chapterId)
    {
        $chapter = Chapter::find($chapterId);
        $support = Support::where('chapter_id', '=', $chapterId)->get();
        return view("pages.chapters.detail", [
            'lang' => $this->lang,
            'chapter' => $chapter,
            'support' => $support,
        ]);
    }
}
