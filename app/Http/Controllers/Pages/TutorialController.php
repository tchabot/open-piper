<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/09/2017
 * Time: 15:09
 */

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Tutorial;

class TutorialController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.tutorials.index", [
            'lang' => $this->lang,
            'tutorials' => Tutorial::all()
        ]);
    }

    public function detail($id)
    {
        return view("pages.tutorials.detail", [
            'lang' => $this->lang,
            'details_tutorials' => Tutorial::find($id)
        ]);
    }
}
