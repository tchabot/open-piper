<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 13/09/2017
 * Time: 14:32
 */

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\User;
use Auth;

class ProfileController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        $user = User::find(Auth::user()->id);
        return view("pages.profile.index", [
            'lang' => $this->lang,
            'user' => $user
        ]);
    }
}