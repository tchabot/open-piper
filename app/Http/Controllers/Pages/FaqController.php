<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        return view("pages.faq.index", [
            'lang' => $this->lang
        ]);
    }
}
