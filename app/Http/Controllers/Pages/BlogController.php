<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 11/09/2017
 * Time: 15:09
 */

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Article;

class BlogController extends Controller
{
    protected $lang;

    public function __construct()
    {
        $this->middleware('guest');
        $this->lang = \App::getLocale();
    }

    public function index()
    {
        $articles = Article::all();
        return view("pages.articles.index", [
            'lang' => $this->lang,
            'articles' => $articles
        ]);
    }

    public function detail($id)
    {
        $article = Article::find($id);
        return view("pages.articles.detail", [
            'lang' => $this->lang,
            'article' => $article
        ]);
    }
}