<?php

function getSeo($field, $page = null)
{
    $lang = App::getLocale();

    if (is_null($page) || empty($page)) {
        $route = Route::getCurrentRoute()->getAction();
        $page = $route['as'];
    }
    $seo = config("routes.$page.$lang.$field");

    if ($field === 'title') {
        $seo = ($page === 'home') ? 'Open Piper | ' . $seo : $seo . ' | Open Piper';
    }

    return $seo;
}