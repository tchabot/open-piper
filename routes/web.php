<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$lang = \App::getLocale();

// Available GET routes
$get = [
    'home',
    'profile',
    'products',
    'product_detail',
    'tutorials',
    'tutorial_detail',
    'articles',
    'article_detail',
    'tos',
    'tou',
    'faq',
    'contact',
    'chapters',
    'chapter_detail'
];

foreach ($get as $page) {
    $controller = config("routes.$page.controller");
    $url = config("routes.$page.$lang.url");
    Route::get($url, $controller)->name($page);
}

Auth::routes();
Route::get('/logout', function(){
    Auth::logout();
    return Redirect::route('home');
});
